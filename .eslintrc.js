module.exports = {
    "extends": [
        "standard",
        "plugin:react/recommended"
    ],
    "env": {
        "browser": true,
        "jest": true
    },
    "parser": "babel-eslint"
};