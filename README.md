# Kumparan Test

A skill test asked by Kumparan.com for front-end position.  

Implementing https://jsonplaceholder.typicode.com to create basic CRUD example.

## How to run

### Locally
`npm start:css` or `yarn start:css` to start css watcher  
`npm start` or `yarn start` to start development server  
Open `localhost:3000`

For a quick preview, check [this link](https://kairxa-kumparan-test.herokuapp.com). Unfortunately, we can still see logger on console due to whatever reason heroku has to not honor NODE_ENV=production.

## Confession

I can't slot enough time to make proper responsive views. Hopefully this is tolerable.  
UX is also very heavily compromised.  
Unit test for components also compromised. Didn't have enough time or energy to finish things up before the deadline.

In any case, thank you for the chance. Hoping for the best now :)