module.exports = {
  'plugins': {
    'tailwindcss': ('./tailwind.config.js'),
    'postcss-aspect-ratio': {},
    'postcss-preset-env': {
      'stage': 0,
      'autoprefixer': {
        'grid': true
      }
    },
    'postcss-browser-reporter': process.env.NODE_ENV === 'production' ? false : {},
    'cssnano': process.env.NODE_ENV === 'production' ? {
      'preset': 'default'
    } : false,
    '@fullhuman/postcss-purgecss': process.env.NODE_ENV === 'production' ? {
      'content': ['./src/**/*.jsx', './src/**/*.js']
    } : false
  }
}
