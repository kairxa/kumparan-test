import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Switch, Redirect } from 'react-router-dom'
import configureStore from './redux/store'
import routes from './route'
import RouteSub from './components/RouteSub'
import Header from './components/Header'

const store = configureStore()

class App extends Component {
  render () {
    return (
      <Provider store={store}>
        <Router>
          <section className="flex flex-col items-center w-full min-h-screen bg-grey-lightest">
            <Header></Header>

            <Switch>
              {routes.map((route, i) => (
                <RouteSub key={i} {...route}></RouteSub>
              ))}
              <Redirect from="*" to="/users"></Redirect>
            </Switch>
          </section>
        </Router>
      </Provider>
    )
  }
}

export default App
