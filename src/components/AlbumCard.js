import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const AlbumCard = ({ album }) => (
  <section className="mb-4 flex flex-col w-full p-3 rounded shadow bg-white">
    <header className="flex items-center w-full">
      <span className="flex-1 font-bold font-sans text-black text-base">Title</span>
      <Link className="font-bold font-sans text-teal text-base no-underline" to={`/album/${album.id}/photos`}>
        Show Photos
      </Link>
    </header>
  </section>
)

AlbumCard.propTypes = {
  album: PropTypes.shape({
    userId: PropTypes.number,
    id: PropTypes.number,
    title: PropTypes.string
  })
}

export default AlbumCard
