import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const CommentCard = ({ comment }) => (
  <section className="mb-4 border-b flex flex-col w-full p-3 rounded shadow bg-white">
    <header className="flex w-full">
      <section className="flex flex-col flex-1">
        <span className="flex-1 font-bold font-sans text-black text-base">{comment.name}</span>
        <span className="mt-2 flex-1 font-sans text-grey text-sm italic">posted by {comment.email}</span>
      </section>
    </header>
    <section className="mt-4 flex flex-col flex-1 w-full">
      <span className="flex-1 font-sans text-black text-base">{comment.body}</span>
    </section>
    <section className="mt-4 flex flex-1 w-full">
      <Link className="font-bold font-sans text-teal text-base no-underline" to={`/comment/${comment.id}/edit`}>Edit</Link>
      <Link className="ml-4 font-bold font-sans text-red text-base no-underline" to={`/comment/${comment.id}/delete`}>Delete</Link>
    </section>
  </section>
)

CommentCard.propTypes = {
  comment: PropTypes.shape({
    id: PropTypes.number,
    postId: PropTypes.number,
    name: PropTypes.string,
    email: PropTypes.string,
    body: PropTypes.string
  })
}

export default CommentCard
