import React from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import PropTypes from 'prop-types'

let CommentForm = ({ handleSubmit, pristine, reset, submitting }) => (
  <form onSubmit={handleSubmit}>
    <div>
      <label htmlFor="name" className="font-sans text-base text-black">Name</label>
      <Field
        component="input"
        type="text"
        name="name"
        id="name"
        className="flex w-full h-10 border-b bg-transparent font-sans text-base px-2 outline-none"
      />
    </div>
    <div className="mt-4">
      <label htmlFor="email" className="font-sans text-base text-black">Email</label>
      <Field
        component="input"
        type="email"
        name="email"
        id="email"
        className="flex w-full h-10 border-b bg-transparent font-sans text-base px-2 outline-none"
      />
    </div>
    <div className="mt-4">
      <label htmlFor="body" className="font-sans text-base text-black">Comment</label>
      <Field
        component="textarea"
        name="body"
        id="body"
        className="mt-4 flex w-full h-24 border-b bg-transparent font-sans text-base px-2 outline-none resize-none"
      ></Field>
    </div>
    <div className="mt-8 flex justify-center">
      <button className="button-Base mod--submit" disabled={pristine || submitting}>
        Submit
      </button>
      <button
        type="reset"
        className="button-Base mod--reset"
        disabled={pristine || submitting}
        onClick={reset}
      >
        Reset
      </button>
    </div>
  </form>
)

CommentForm = reduxForm({
  form: 'comment',
  enableReinitialize: true
})(CommentForm)

CommentForm.propTypes = {
  handleSubmit: PropTypes.func,
  initialValues: PropTypes.shape({
    id: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    name: PropTypes.string,
    body: PropTypes.string,
    email: PropTypes.string
  }),
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  reset: PropTypes.func
}

const mapStateToProps = ({ comment }) => ({ initialValues: comment.details })

export default connect(mapStateToProps)(CommentForm)
