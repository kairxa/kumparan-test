import React from 'react'
import { Link, NavLink } from 'react-router-dom'

const Header = () => (
  <nav className="flex items-center justify-center w-full h-14 p-3 bg-grey-lighter shadow">
    <section className="flex w-full max-w-xl h-full">
      <div className="flex flex-1 items-center">
        <Link to="/" className="font-sans text-2xl no-underline text-teal font-bold">Kumparan</Link>
      </div>
      <div className="flex items-center justify-center">
        <NavLink activeClassName="font-bold" className="font-sans text-lg text-black no-underline" to="/users">Users</NavLink>
      </div>
    </section>
  </nav>
)

export default Header
