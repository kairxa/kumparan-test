import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const PhotoThumb = ({ photo }) => (
  <Link className="mb-3 flex mr-2 min-w-32 h-32" to={`/photo/${photo.id}`}>
    <img src={photo.thumbnailUrl} alt="Name" className="block"/>
  </Link>
)

PhotoThumb.propTypes = {
  photo: PropTypes.shape({
    id: PropTypes.number,
    albumId: PropTypes.number,
    url: PropTypes.string,
    thumbnailUrl: PropTypes.string,
    title: PropTypes.string
  })
}

export default PhotoThumb
