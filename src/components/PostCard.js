import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const PostCard = ({ post }) => (
  <section className="mb-4 border-b flex flex-col w-full p-3 lg:rounded shadow bg-white">
    <header className="flex items-center w-full">
      <span className="flex-1 font-bold font-sans text-black text-base">{ post.title }</span>
    </header>
    <div className="flex-1 w-full mt-3">
      <span className="font-sans text-base text-black">{ post.body }</span>
    </div>
    <div className="flex-1 w-full mt-3">
      <Link className="font-bold font-sans text-teal text-base no-underline" to={`/post/${post.id}/comments`}>
        Show Comments
      </Link>
      <Link className="ml-4 font-bold font-sans text-teal text-base no-underline" to="/comment/add">Add Comment</Link>
      <Link className="ml-4 font-bold font-sans text-teal text-base no-underline" to={`/post/${post.id}/edit`}>Edit</Link>
      <Link className="ml-4 font-bold font-sans text-red text-base no-underline" to={`/post/${post.id}/delete`}>Delete</Link>
    </div>
  </section>
)

PostCard.propTypes = {
  post: PropTypes.shape({
    userId: PropTypes.number,
    id: PropTypes.number,
    title: PropTypes.string,
    body: PropTypes.string
  })
}

export default PostCard
