import React from 'react'
import { Route } from 'react-router-dom'

const RouteSub = (route) => (
  <Route
    path={route.path}
    exact={route.exact}
    render={(props) => (
      <route.component {...props} routes={route.routes}></route.component>
    )}
  >
  </Route>
)

export default RouteSub
