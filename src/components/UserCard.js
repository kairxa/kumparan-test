import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const UserCard = ({ user }) => (
  <section className="mb-4 flex flex-col w-full min-h-24 p-3 rounded shadow bg-white">
    <header className="flex w-full">
      <div className="flex flex-col flex-1">
        <Link className="flex-1 font-bold font-sans text-black text-base no-underline" to={`/user/${user.id}`}>{user.username}</Link>
        <span className="mt-2 flex-1 font-sans text-grey text-sm">{user.name} - {user.email}</span>
      </div>
      <div className="flex">
        <Link className="font-bold font-sans text-teal text-base no-underline" to={`/user/${user.id}/albums`}>Show Albums</Link>
        <Link className="font-bold font-sans text-teal text-base no-underline ml-4" to={`/user/${user.id}/posts`}>Show Posts</Link>
      </div>
    </header>
    <section className="mt-4 flex w-full">
      <div className="flex flex-1 flex-col">
        <span className="flex text-black text-sm">Phone: {user.phone}</span>
        <span className="flex text-black text-sm mt-2">Website: {user.website}</span>
        <span className="flex text-black text-sm mt-2">Address: {user.address.street}, {user.address.suite}, {user.address.city}</span>
      </div>
      <div className="flex flex-1 flex-col items-end">
        <span className="flex text-black text-sm">{user.company.name}</span>
        <span className="flex text-black text-sm mt-2">{user.company.catchPhrase}</span>
        <span className="flex text-black text-sm mt-2">{user.company.bs}</span>
      </div>
    </section>
  </section>
)

UserCard.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    username: PropTypes.string,
    email: PropTypes.string,
    address: PropTypes.shape({
      street: PropTypes.string,
      suite: PropTypes.string,
      city: PropTypes.string
    }),
    phone: PropTypes.string,
    website: PropTypes.string,
    company: PropTypes.shape({
      name: PropTypes.string,
      catchPhrase: PropTypes.string,
      bs: PropTypes.string
    })
  })
}

export default UserCard
