import catchify from 'catchify'
import {
  ALBUM_LIST_REQUEST,
  ALBUM_LIST_RECEIVE,
  ALBUM_DETAIL_REQUEST,
  ALBUM_DETAIL_RECEIVE
} from '../types'
import failure from './failure'

const BASE_URL = 'https://jsonplaceholder.typicode.com'

const albumsRequest = (userId) => ({
  type: ALBUM_LIST_REQUEST,
  userId
})

const albumsReceive = (userId, albums) => ({
  type: ALBUM_LIST_RECEIVE,
  userId,
  albums
})

export const albumsFetch = (userId) => {
  return async (dispatch) => {
    dispatch(albumsRequest(userId))

    const [err, response] = await catchify(fetch(`${BASE_URL}/users/${userId}/albums`))

    if (err) {
      dispatch(failure(ALBUM_LIST_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(failure(ALBUM_LIST_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(albumsReceive(userId, data))
    }
  }
}

const albumRequest = (albumId) => ({
  type: ALBUM_DETAIL_REQUEST,
  albumId
})

const albumReceive = (albumId, albumDetails) => ({
  type: ALBUM_DETAIL_RECEIVE,
  albumId,
  details: albumDetails
})

export const albumFetch = (albumId) => {
  return async (dispatch) => {
    dispatch(albumRequest(albumId))

    const [err, response] = await catchify(fetch(`${BASE_URL}/albums/${albumId}`))

    if (err) {
      dispatch(failure(ALBUM_DETAIL_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(failure(ALBUM_DETAIL_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(albumReceive(albumId, data))
    }
  }
}
