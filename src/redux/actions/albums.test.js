import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import { albumsFetch, albumFetch } from './albums'
import { ALBUM_LIST_REQUEST, ALBUM_LIST_RECEIVE, ALBUM_DETAIL_REQUEST, ALBUM_DETAIL_RECEIVE, ACTION_FAILURE } from '../types'

const BASE_URL = 'https://jsonplaceholder.typicode.com'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Albums fetch', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates ALBUM_LIST_RECEIVE when fetching albums is done succesfully', () => {
    const albums = [{
      'userId': 1,
      'id': 1,
      'title': 'quidem molestiae enim'
    }]

    fetchMock.getOnce(`${BASE_URL}/users/1/albums`, albums)

    const expectedActions = [
      { type: ALBUM_LIST_REQUEST, userId: 1 },
      { type: ALBUM_LIST_RECEIVE, albums, userId: 1 }
    ]

    const store = mockStore({ albums: [] })

    return store.dispatch(albumsFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    fetchMock
      .getOnce(`${BASE_URL}/users/1/albums`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: ALBUM_LIST_REQUEST, userId: 1 },
      { type: ACTION_FAILURE, during: ALBUM_LIST_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ albums: [] })

    return store.dispatch(albumsFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    fetchMock
      .getOnce(`${BASE_URL}/users/1/albums`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: ALBUM_LIST_REQUEST, userId: 1 },
      { type: ACTION_FAILURE, during: ALBUM_LIST_REQUEST, message: `invalid json response body at ${BASE_URL}/users/1/albums reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ albums: [] })

    return store.dispatch(albumsFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe('Album fetch', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates ALBUM_DETAIL_RECEIVE when everything is ok', () => {
    const album = {
      'userId': 1,
      'id': 1,
      'title': 'quidem molestiae enim'
    }

    fetchMock.getOnce(`${BASE_URL}/albums/1`, album)

    const expectedActions = [
      { type: ALBUM_DETAIL_REQUEST, albumId: 1 },
      { type: ALBUM_DETAIL_RECEIVE, albumId: 1, details: album }
    ]

    const store = mockStore({ type: '', albumId: 0, details: {} })

    return store.dispatch(albumFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    fetchMock
      .getOnce(`${BASE_URL}/albums/1`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: ALBUM_DETAIL_REQUEST, albumId: 1 },
      { type: ACTION_FAILURE, during: ALBUM_DETAIL_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ type: '', albumId: 0, details: {} })

    return store.dispatch(albumFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    fetchMock
      .getOnce(`${BASE_URL}/albums/1`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: ALBUM_DETAIL_REQUEST, albumId: 1 },
      { type: ACTION_FAILURE, during: ALBUM_DETAIL_REQUEST, message: `invalid json response body at ${BASE_URL}/albums/1 reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ type: '', albumId: 0, details: {} })

    return store.dispatch(albumFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
