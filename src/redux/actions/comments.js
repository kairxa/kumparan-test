import catchify from 'catchify'
import {
  COMMENTS_REQUEST,
  COMMENTS_RECEIVE,
  COMMENT_REQUEST,
  COMMENT_RECEIVE,
  COMMENT_ADD_REQUEST,
  COMMENT_ADD_RECEIVE,
  COMMENT_PATCH_REQUEST,
  COMMENT_PATCH_RECEIVE,
  COMMENT_DELETE_REQUEST,
  COMMENT_DELETE_RECEIVE
} from '../types'
import failure from './failure'

const BASE_URL = 'https://jsonplaceholder.typicode.com'

const commentsRequest = (postId) => ({
  type: COMMENTS_REQUEST,
  postId
})

const commentsReceive = (postId, comments) => ({
  type: COMMENTS_RECEIVE,
  postId,
  comments
})

export const commentsFetch = (postId) => {
  return async (dispatch) => {
    dispatch(commentsRequest(postId))

    const [err, response] = await catchify(fetch(`${BASE_URL}/posts/${postId}/comments`))

    if (err) {
      dispatch(failure(COMMENTS_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(failure(COMMENTS_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(commentsReceive(postId, data))
    }
  }
}

const commentRequest = (commentId) => ({
  type: COMMENT_REQUEST,
  commentId
})

const commentReceive = (commentId, commentDetails) => ({
  type: COMMENT_RECEIVE,
  commentId,
  details: commentDetails
})

export const commentFetch = (commentId) => {
  return async (dispatch) => {
    dispatch(commentRequest(commentId))

    const [err, response] = await catchify(fetch(`${BASE_URL}/comments/${commentId}`))

    if (err) {
      dispatch(failure(COMMENT_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(failure(COMMENT_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(commentReceive(commentId, data))
    }
  }
}

const commentAddRequest = (details, inProgress = true, failed = false) => ({
  type: COMMENT_ADD_REQUEST,
  details,
  inProgress,
  success: false,
  failed
})

const commentAddReceive = (details) => ({
  type: COMMENT_ADD_RECEIVE,
  commentId: details.id,
  details,
  inProgress: false,
  success: true,
  failed: false
})

export const commentAdd = (details) => {
  return async (dispatch) => {
    dispatch(commentAddRequest(details))

    const [err, response] = await catchify(fetch(`${BASE_URL}/comments`, {
      method: 'POST',
      body: JSON.stringify(details),
      headers: {
        'Content-type': 'application/json; charset=UTF-8'
      }
    }))

    if (err) {
      dispatch(commentAddRequest(details, false, true))
      dispatch(failure(COMMENT_ADD_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(commentAddRequest(details, false, true))
      dispatch(failure(COMMENT_ADD_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(commentAddReceive(data))
    }
  }
}

const commentUpdateRequest = (details, inProgress = true, failed = false) => ({
  type: COMMENT_PATCH_REQUEST,
  commentId: details.id,
  details,
  inProgress,
  success: false,
  failed
})

const commentUpdateReceive = (details) => ({
  type: COMMENT_PATCH_RECEIVE,
  commentId: details.id,
  details,
  inProgress: false,
  success: true,
  failed: false
})

export const commentUpdate = (details) => {
  return async (dispatch) => {
    dispatch(commentUpdateRequest(details))

    const [err, response] = await catchify(fetch(`${BASE_URL}/comments/${details.id}`, {
      method: 'PATCH',
      body: JSON.stringify(details),
      headers: {
        'Content-type': 'application/json; charset=UTF-8'
      }
    }))

    if (err) {
      dispatch(commentUpdateRequest(details, false, true))
      dispatch(failure(COMMENT_PATCH_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(commentUpdateRequest(details, false, true))
      dispatch(failure(COMMENT_PATCH_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(commentUpdateReceive(data))
    }
  }
}

const commentDeleteRequest = (commentId, inProgress = true, failed = false) => ({
  type: COMMENT_DELETE_REQUEST,
  commentId,
  inProgress,
  success: false,
  failed
})

const commentDeleteReceive = (commentId) => ({
  type: COMMENT_DELETE_RECEIVE,
  commentId,
  inProgress: false,
  success: true,
  failed: false
})

export const commentDelete = (commentId) => {
  return async (dispatch) => {
    dispatch(commentDeleteRequest(commentId))

    const [err, response] = await catchify(fetch(`${BASE_URL}/comments/${commentId}`, {
      method: 'DELETE'
    }))

    if (err) {
      dispatch(commentDeleteRequest(commentId, false, true))
      dispatch(failure(COMMENT_DELETE_REQUEST, err.message))
      return
    }

    if (response.status === 200) {
      dispatch(commentDeleteReceive(commentId))
    } else {
      dispatch(commentDeleteRequest(commentId, false, true))
      dispatch(failure(COMMENT_DELETE_REQUEST, response.status))
    }
  }
}
