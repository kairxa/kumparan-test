import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import { commentsFetch, commentFetch, commentAdd, commentUpdate, commentDelete } from './comments'
import { COMMENTS_REQUEST, COMMENTS_RECEIVE, COMMENT_REQUEST, COMMENT_RECEIVE, ACTION_FAILURE, COMMENT_ADD_REQUEST, COMMENT_ADD_RECEIVE, COMMENT_PATCH_REQUEST, COMMENT_PATCH_RECEIVE, COMMENT_DELETE_REQUEST, COMMENT_DELETE_RECEIVE } from '../types'

const BASE_URL = 'https://jsonplaceholder.typicode.com'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Comments fetch', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates COMMENTS_RECEIVE when fetching comments is done succesfully', () => {
    const comments = [{
      id: 1,
      title: 'Best Title',
      body: 'Best body',
      postId: 1
    }]

    fetchMock.getOnce(`${BASE_URL}/posts/1/comments`, comments)

    const expectedActions = [
      { type: COMMENTS_REQUEST, postId: 1 },
      { type: COMMENTS_RECEIVE, comments, postId: 1 }
    ]

    const store = mockStore({ comments: [] })

    return store.dispatch(commentsFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server throws error', () => {
    fetchMock.getOnce(`${BASE_URL}/posts/1/comments`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: COMMENTS_REQUEST, postId: 1 },
      { type: ACTION_FAILURE, during: COMMENTS_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ comments: [] })

    return store.dispatch(commentsFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    fetchMock.getOnce(`${BASE_URL}/posts/1/comments`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: COMMENTS_REQUEST, postId: 1 },
      { type: ACTION_FAILURE, during: COMMENTS_REQUEST, message: `invalid json response body at ${BASE_URL}/posts/1/comments reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ comments: [] })

    return store.dispatch(commentsFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe('Comment fetch', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates COMMENT_RECEIVE when everything is ok', () => {
    const comment = {
      'postId': 1,
      'id': 1,
      'title': 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
      'body': 'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto'
    }

    fetchMock.getOnce(`${BASE_URL}/comments/1`, comment)

    const expectedActions = [
      { type: COMMENT_REQUEST, commentId: 1 },
      { type: COMMENT_RECEIVE, commentId: 1, details: comment }
    ]

    const store = mockStore({ type: '', commentId: 0, details: {} })

    return store.dispatch(commentFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    fetchMock.getOnce(`${BASE_URL}/comments/1`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: COMMENT_REQUEST, commentId: 1 },
      { type: ACTION_FAILURE, during: COMMENT_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ type: '', commentId: 0, details: {} })

    return store.dispatch(commentFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    fetchMock.getOnce(`${BASE_URL}/comments/1`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: COMMENT_REQUEST, commentId: 1 },
      { type: ACTION_FAILURE, during: COMMENT_REQUEST, message: `invalid json response body at ${BASE_URL}/comments/1 reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ type: '', commentId: 0, details: {} })

    return store.dispatch(commentFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe('Comment create', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates COMMENT_ADD_RECEIVE when comment add ran succesfully', () => {
    const comment = {
      title: 'BEST TITLE',
      body: 'BEST BODY',
      postId: 1
    }

    fetchMock.postOnce(`${BASE_URL}/comments`, { ...comment, id: 1 })

    const expectedActions = [
      { type: COMMENT_ADD_REQUEST, details: comment, inProgress: true, success: false, failed: false },
      { type: COMMENT_ADD_RECEIVE, details: { ...comment, id: 1 }, commentId: 1, inProgress: false, success: true, failed: false }
    ]

    const store = mockStore({ type: '', details: {}, commentId: 0 })

    return store.dispatch(commentAdd(comment)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    const comment = {
      title: 'BEST TITLE',
      body: 'BEST BODY',
      postId: 1
    }

    fetchMock.postOnce(`${BASE_URL}/comments`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: COMMENT_ADD_REQUEST, details: comment, inProgress: true, success: false, failed: false },
      { type: COMMENT_ADD_REQUEST, details: comment, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: COMMENT_ADD_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ type: '', details: {}, commentId: 0 })

    return store.dispatch(commentAdd(comment)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    const comment = {
      title: 'BEST TITLE',
      body: 'BEST BODY',
      postId: 1
    }

    fetchMock.postOnce(`${BASE_URL}/comments`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: COMMENT_ADD_REQUEST, details: comment, inProgress: true, success: false, failed: false },
      { type: COMMENT_ADD_REQUEST, details: comment, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: COMMENT_ADD_REQUEST, message: `invalid json response body at ${BASE_URL}/comments reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ type: '', details: {}, commentId: 0 })

    return store.dispatch(commentAdd(comment)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe('Comment update', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates COMMENT_PATCH_RECEIVE when comment add ran succesfully', () => {
    const comment = {
      id: 1,
      title: 'VERY BEST TITLE'
    }

    fetchMock.patchOnce(`${BASE_URL}/comments/${comment.id}`, { ...comment, body: 'BEST BODY', postId: 1 })

    const expectedActions = [
      { type: COMMENT_PATCH_REQUEST, details: comment, commentId: comment.id, inProgress: true, success: false, failed: false },
      {
        type: COMMENT_PATCH_RECEIVE,
        details: { ...comment, body: 'BEST BODY', postId: 1 },
        commentId: comment.id,
        inProgress: false,
        success: true,
        failed: false
      }
    ]

    const store = mockStore({ type: '', details: {}, commentId: 0 })

    return store.dispatch(commentUpdate(comment)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    const comment = {
      id: 1,
      title: 'VERY BEST TITLE'
    }

    fetchMock.patchOnce(`${BASE_URL}/comments/${comment.id}`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: COMMENT_PATCH_REQUEST, details: comment, commentId: comment.id, inProgress: true, success: false, failed: false },
      { type: COMMENT_PATCH_REQUEST, details: comment, commentId: comment.id, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: COMMENT_PATCH_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ type: '', details: {}, commentId: 0 })

    return store.dispatch(commentUpdate(comment)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    const comment = {
      id: 1,
      title: 'VERY BEST TITLE'
    }

    fetchMock.patchOnce(`${BASE_URL}/comments/${comment.id}`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: COMMENT_PATCH_REQUEST, details: comment, commentId: comment.id, inProgress: true, success: false, failed: false },
      { type: COMMENT_PATCH_REQUEST, details: comment, commentId: comment.id, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: COMMENT_PATCH_REQUEST, message: `invalid json response body at ${BASE_URL}/comments/${comment.id} reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ type: '', details: {}, commentId: 0 })

    return store.dispatch(commentUpdate(comment)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe('Comment delete', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates COMMENT_DELETE_RECEIVE when comment add ran succesfully', () => {
    fetchMock.deleteOnce(`${BASE_URL}/comments/1`, 200)

    const expectedActions = [
      { type: COMMENT_DELETE_REQUEST, commentId: 1, inProgress: true, success: false, failed: false },
      { type: COMMENT_DELETE_RECEIVE, commentId: 1, inProgress: false, success: true, failed: false }
    ]

    const store = mockStore({ type: '', details: {}, commentId: 0 })

    return store.dispatch(commentDelete(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    fetchMock.deleteOnce(`${BASE_URL}/comments/1`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: COMMENT_DELETE_REQUEST, commentId: 1, inProgress: true, success: false, failed: false },
      { type: COMMENT_DELETE_REQUEST, commentId: 1, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: COMMENT_DELETE_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ type: '', details: {}, commentId: 0 })

    return store.dispatch(commentDelete(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns non 200', () => {
    fetchMock.deleteOnce(`${BASE_URL}/comments/1`, 402)

    const expectedActions = [
      { type: COMMENT_DELETE_REQUEST, commentId: 1, inProgress: true, success: false, failed: false },
      { type: COMMENT_DELETE_REQUEST, commentId: 1, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: COMMENT_DELETE_REQUEST, message: 402 }
    ]

    const store = mockStore({ type: '', details: {}, commentId: 0 })

    return store.dispatch(commentDelete(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
