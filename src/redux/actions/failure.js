import { ACTION_FAILURE } from '../types'

export default (type, message) => ({
  type: ACTION_FAILURE,
  during: type,
  message
})
