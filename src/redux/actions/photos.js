import catchify from 'catchify'
import {
  PHOTOS_REQUEST,
  PHOTOS_RECEIVE,
  PHOTO_REQUEST,
  PHOTO_RECEIVE
} from '../types'
import failure from './failure'

const BASE_URL = 'https://jsonplaceholder.typicode.com'

const photosRequest = (albumId) => ({
  type: PHOTOS_REQUEST,
  albumId
})

const photosReceive = (albumId, photos) => ({
  type: PHOTOS_RECEIVE,
  albumId,
  photos
})

export const photosFetch = (albumId) => {
  return async (dispatch) => {
    dispatch(photosRequest(albumId))

    const [err, response] = await catchify(fetch(`${BASE_URL}/albums/${albumId}/photos`))

    if (err) {
      dispatch(failure(PHOTOS_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(failure(PHOTOS_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(photosReceive(albumId, data))
    }
  }
}

const photoRequest = (photoId) => ({
  type: PHOTO_REQUEST,
  photoId
})

const photoReceive = (photoId, photoDetails) => ({
  type: PHOTO_RECEIVE,
  photoId,
  details: photoDetails
})

export const photoFetch = (photoId) => {
  return async (dispatch) => {
    dispatch(photoRequest(photoId))

    const [err, response] = await catchify(fetch(`${BASE_URL}/photos/${photoId}`))

    if (err) {
      dispatch(failure(PHOTO_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(failure(PHOTO_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(photoReceive(photoId, data))
    }
  }
}
