import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import { photosFetch, photoFetch } from './photos'
import { PHOTOS_REQUEST, PHOTOS_RECEIVE, PHOTO_REQUEST, PHOTO_RECEIVE, ACTION_FAILURE } from '../types'

const BASE_URL = 'https://jsonplaceholder.typicode.com'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Photos fetch', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates PHOTOS_RECEIVE when fetching photos is done succesfully', () => {
    const photos = [{
      'albumId': 1,
      'id': 1,
      'title': 'accusamus beatae ad facilis cum similique qui sunt',
      'url': 'https://via.placeholder.com/600/92c952',
      'thumbnailUrl': 'https://via.placeholder.com/150/92c952'
    }]

    fetchMock.getOnce(`${BASE_URL}/albums/1/photos`, photos)

    const expectedActions = [
      { type: PHOTOS_REQUEST, albumId: 1 },
      { type: PHOTOS_RECEIVE, photos, albumId: 1 }
    ]

    const store = mockStore({ photos: [] })

    return store.dispatch(photosFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    fetchMock
      .getOnce(`${BASE_URL}/albums/1/photos`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: PHOTOS_REQUEST, albumId: 1 },
      { type: ACTION_FAILURE, during: PHOTOS_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ photos: [] })

    return store.dispatch(photosFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    fetchMock
      .getOnce(`${BASE_URL}/albums/1/photos`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: PHOTOS_REQUEST, albumId: 1 },
      { type: ACTION_FAILURE, during: PHOTOS_REQUEST, message: `invalid json response body at ${BASE_URL}/albums/1/photos reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ photos: [] })

    return store.dispatch(photosFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe('Photo fetch', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates PHOTO_RECEIVE when everything is ok', () => {
    const photo = {
      'albumId': 1,
      'id': 1,
      'title': 'accusamus beatae ad facilis cum similique qui sunt',
      'url': 'https://via.placeholder.com/600/92c952',
      'thumbnailUrl': 'https://via.placeholder.com/150/92c952'
    }

    fetchMock.getOnce(`${BASE_URL}/photos/1`, photo)

    const expectedActions = [
      { type: PHOTO_REQUEST, photoId: 1 },
      { type: PHOTO_RECEIVE, photoId: 1, details: photo }
    ]

    const store = mockStore({ type: '', photoId: 0, details: {} })

    return store.dispatch(photoFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    fetchMock
      .getOnce(`${BASE_URL}/photos/1`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: PHOTO_REQUEST, photoId: 1 },
      { type: ACTION_FAILURE, during: PHOTO_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ type: '', photoId: 0, details: {} })

    return store.dispatch(photoFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    fetchMock
      .getOnce(`${BASE_URL}/photos/1`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: PHOTO_REQUEST, photoId: 1 },
      { type: ACTION_FAILURE, during: PHOTO_REQUEST, message: `invalid json response body at ${BASE_URL}/photos/1 reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ type: '', photoId: 0, details: {} })

    return store.dispatch(photoFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
