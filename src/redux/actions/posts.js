import catchify from 'catchify'
import {
  POSTS_REQUEST,
  POSTS_RECEIVE,
  POST_REQUEST,
  POST_RECEIVE,
  POST_ADD_REQUEST,
  POST_ADD_RECEIVE,
  POST_PATCH_REQUEST,
  POST_PATCH_RECEIVE,
  POST_DELETE_REQUEST,
  POST_DELETE_RECEIVE
} from '../types'
import failure from './failure'

const BASE_URL = 'https://jsonplaceholder.typicode.com'

const postsRequest = (userId) => ({
  type: POSTS_REQUEST,
  userId
})

const postsReceive = (userId, posts) => ({
  type: POSTS_RECEIVE,
  userId,
  posts
})

export const postsFetch = (userId) => {
  return async (dispatch) => {
    dispatch(postsRequest(userId))

    const [err, response] = await catchify(fetch(`${BASE_URL}/users/${userId}/posts`))

    if (err) {
      dispatch(failure(POSTS_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(failure(POSTS_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(postsReceive(userId, data))
    }
  }
}

const postRequest = (postId) => ({
  type: POST_REQUEST,
  postId
})

const postReceive = (postId, postDetails) => ({
  type: POST_RECEIVE,
  postId,
  details: postDetails
})

export const postFetch = (postId) => {
  return async (dispatch) => {
    dispatch(postRequest(postId))

    const [err, response] = await catchify(fetch(`${BASE_URL}/posts/${postId}`))

    if (err) {
      dispatch(failure(POST_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(failure(POST_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(postReceive(postId, data))
    }
  }
}

const postAddRequest = (details, inProgress = true, failed = false) => ({
  type: POST_ADD_REQUEST,
  details,
  inProgress,
  success: false,
  failed
})

const postAddReceive = (details) => ({
  type: POST_ADD_RECEIVE,
  postId: details.id,
  details,
  inProgress: false,
  success: true,
  failed: false
})

export const postAdd = (details) => {
  return async (dispatch) => {
    dispatch(postAddRequest(details))

    const [err, response] = await catchify(fetch(`${BASE_URL}/posts`, {
      method: 'POST',
      body: JSON.stringify(details),
      headers: {
        'Content-type': 'application/json; charset=UTF-8'
      }
    }))

    if (err) {
      dispatch(postAddRequest(details, false, true))
      dispatch(failure(POST_ADD_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(postAddRequest(details, false, true))
      dispatch(failure(POST_ADD_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(postAddReceive(data))
    }
  }
}

const postUpdateRequest = (details, inProgress = true, failed = false) => ({
  type: POST_PATCH_REQUEST,
  postId: details.id,
  details,
  inProgress,
  success: false,
  failed
})

const postUpdateReceive = (details) => ({
  type: POST_PATCH_RECEIVE,
  postId: details.id,
  details,
  inProgress: false,
  success: true,
  failed: false
})

export const postUpdate = (details) => {
  return async (dispatch) => {
    dispatch(postUpdateRequest(details))

    const [err, response] = await catchify(fetch(`${BASE_URL}/posts/${details.id}`, {
      method: 'PATCH',
      body: JSON.stringify(details),
      headers: {
        'Content-type': 'application/json; charset=UTF-8'
      }
    }))

    if (err) {
      dispatch(postUpdateRequest(details, false, true))
      dispatch(failure(POST_PATCH_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(postUpdateRequest(details, false, true))
      dispatch(failure(POST_PATCH_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(postUpdateReceive(data))
    }
  }
}

const postDeleteRequest = (postId, inProgress = true, failed = false) => ({
  type: POST_DELETE_REQUEST,
  postId,
  inProgress,
  success: false,
  failed
})

const postDeleteReceive = (postId) => ({
  type: POST_DELETE_RECEIVE,
  postId,
  inProgress: false,
  success: true,
  failed: false
})

export const postDelete = (postId) => {
  return async (dispatch) => {
    dispatch(postDeleteRequest(postId))

    const [err, response] = await catchify(fetch(`${BASE_URL}/posts/${postId}`, {
      method: 'DELETE'
    }))

    if (err) {
      dispatch(postDeleteRequest(postId, false, true))
      dispatch(failure(POST_DELETE_REQUEST, err.message))
      return
    }

    if (response.status === 200) {
      dispatch(postDeleteReceive(postId))
    } else {
      dispatch(postDeleteRequest(postId, false, true))
      dispatch(failure(POST_DELETE_REQUEST, response.status))
    }
  }
}
