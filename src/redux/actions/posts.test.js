import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import { postsFetch, postFetch, postAdd, postUpdate, postDelete } from './posts'
import { POSTS_REQUEST, POSTS_RECEIVE, POST_REQUEST, POST_RECEIVE, ACTION_FAILURE, POST_ADD_REQUEST, POST_ADD_RECEIVE, POST_PATCH_REQUEST, POST_PATCH_RECEIVE, POST_DELETE_REQUEST, POST_DELETE_RECEIVE } from '../types'

const BASE_URL = 'https://jsonplaceholder.typicode.com'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Posts fetch', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates POSTS_RECEIVE when fetching posts is done succesfully', () => {
    const posts = [{
      id: 1,
      title: 'Best Title',
      body: 'Best body',
      userId: 1
    }]

    fetchMock.getOnce(`${BASE_URL}/users/1/posts`, posts)

    const expectedActions = [
      { type: POSTS_REQUEST, userId: 1 },
      { type: POSTS_RECEIVE, posts, userId: 1 }
    ]

    const store = mockStore({ posts: [] })

    return store.dispatch(postsFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server throws error', () => {
    fetchMock.getOnce(`${BASE_URL}/users/1/posts`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: POSTS_REQUEST, userId: 1 },
      { type: ACTION_FAILURE, during: POSTS_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ posts: [] })

    return store.dispatch(postsFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    fetchMock.getOnce(`${BASE_URL}/users/1/posts`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: POSTS_REQUEST, userId: 1 },
      { type: ACTION_FAILURE, during: POSTS_REQUEST, message: `invalid json response body at ${BASE_URL}/users/1/posts reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ posts: [] })

    return store.dispatch(postsFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe('Post fetch', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates POST_RECEIVE when everything is ok', () => {
    const post = {
      'userId': 1,
      'id': 1,
      'title': 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
      'body': 'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto'
    }

    fetchMock.getOnce(`${BASE_URL}/posts/1`, post)

    const expectedActions = [
      { type: POST_REQUEST, postId: 1 },
      { type: POST_RECEIVE, postId: 1, details: post }
    ]

    const store = mockStore({ type: '', postId: 0, details: {} })

    return store.dispatch(postFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    fetchMock.getOnce(`${BASE_URL}/posts/1`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: POST_REQUEST, postId: 1 },
      { type: ACTION_FAILURE, during: POST_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ type: '', postId: 0, details: {} })

    return store.dispatch(postFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    fetchMock.getOnce(`${BASE_URL}/posts/1`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: POST_REQUEST, postId: 1 },
      { type: ACTION_FAILURE, during: POST_REQUEST, message: `invalid json response body at ${BASE_URL}/posts/1 reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ type: '', postId: 0, details: {} })

    return store.dispatch(postFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe('Post create', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates POST_ADD_RECEIVE when post add ran succesfully', () => {
    const post = {
      title: 'BEST TITLE',
      body: 'BEST BODY',
      userId: 1
    }

    fetchMock.postOnce(`${BASE_URL}/posts`, { ...post, id: 1 })

    const expectedActions = [
      { type: POST_ADD_REQUEST, details: post, inProgress: true, success: false, failed: false },
      { type: POST_ADD_RECEIVE, details: { ...post, id: 1 }, postId: 1, inProgress: false, success: true, failed: false }
    ]

    const store = mockStore({ type: '', details: {}, postId: 0 })

    return store.dispatch(postAdd(post)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    const post = {
      title: 'BEST TITLE',
      body: 'BEST BODY',
      userId: 1
    }

    fetchMock.postOnce(`${BASE_URL}/posts`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: POST_ADD_REQUEST, details: post, inProgress: true, success: false, failed: false },
      { type: POST_ADD_REQUEST, details: post, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: POST_ADD_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ type: '', details: {}, postId: 0 })

    return store.dispatch(postAdd(post)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    const post = {
      title: 'BEST TITLE',
      body: 'BEST BODY',
      userId: 1
    }

    fetchMock.postOnce(`${BASE_URL}/posts`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: POST_ADD_REQUEST, details: post, inProgress: true, success: false, failed: false },
      { type: POST_ADD_REQUEST, details: post, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: POST_ADD_REQUEST, message: `invalid json response body at ${BASE_URL}/posts reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ type: '', details: {}, postId: 0 })

    return store.dispatch(postAdd(post)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe('Post update', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates POST_PATCH_RECEIVE when post add ran succesfully', () => {
    const post = {
      id: 1,
      title: 'VERY BEST TITLE'
    }

    fetchMock.patchOnce(`${BASE_URL}/posts/${post.id}`, { ...post, body: 'BEST BODY', userId: 1 })

    const expectedActions = [
      { type: POST_PATCH_REQUEST, details: post, postId: post.id, inProgress: true, success: false, failed: false },
      {
        type: POST_PATCH_RECEIVE,
        details: { ...post, body: 'BEST BODY', userId: 1 },
        postId: post.id,
        inProgress: false,
        success: true,
        failed: false
      }
    ]

    const store = mockStore({ type: '', details: {}, postId: 0 })

    return store.dispatch(postUpdate(post)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    const post = {
      id: 1,
      title: 'VERY BEST TITLE'
    }

    fetchMock.patchOnce(`${BASE_URL}/posts/${post.id}`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: POST_PATCH_REQUEST, details: post, postId: post.id, inProgress: true, success: false, failed: false },
      { type: POST_PATCH_REQUEST, details: post, postId: post.id, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: POST_PATCH_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ type: '', details: {}, postId: 0 })

    return store.dispatch(postUpdate(post)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    const post = {
      id: 1,
      title: 'VERY BEST TITLE'
    }

    fetchMock.patchOnce(`${BASE_URL}/posts/${post.id}`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: POST_PATCH_REQUEST, details: post, postId: post.id, inProgress: true, success: false, failed: false },
      { type: POST_PATCH_REQUEST, details: post, postId: post.id, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: POST_PATCH_REQUEST, message: `invalid json response body at ${BASE_URL}/posts/${post.id} reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ type: '', details: {}, postId: 0 })

    return store.dispatch(postUpdate(post)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe('Post delete', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates POST_DELETE_RECEIVE when post add ran succesfully', () => {
    fetchMock.deleteOnce(`${BASE_URL}/posts/1`, 200)

    const expectedActions = [
      { type: POST_DELETE_REQUEST, postId: 1, inProgress: true, success: false, failed: false },
      { type: POST_DELETE_RECEIVE, postId: 1, inProgress: false, success: true, failed: false }
    ]

    const store = mockStore({ type: '', details: {}, postId: 0 })

    return store.dispatch(postDelete(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    fetchMock.deleteOnce(`${BASE_URL}/posts/1`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: POST_DELETE_REQUEST, postId: 1, inProgress: true, success: false, failed: false },
      { type: POST_DELETE_REQUEST, postId: 1, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: POST_DELETE_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ type: '', details: {}, postId: 0 })

    return store.dispatch(postDelete(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns non 200', () => {
    fetchMock.deleteOnce(`${BASE_URL}/posts/1`, 402)

    const expectedActions = [
      { type: POST_DELETE_REQUEST, postId: 1, inProgress: true, success: false, failed: false },
      { type: POST_DELETE_REQUEST, postId: 1, inProgress: false, success: false, failed: true },
      { type: ACTION_FAILURE, during: POST_DELETE_REQUEST, message: 402 }
    ]

    const store = mockStore({ type: '', details: {}, postId: 0 })

    return store.dispatch(postDelete(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
