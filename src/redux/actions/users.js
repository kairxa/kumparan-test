import catchify from 'catchify'
import {
  USERS_REQUEST,
  USERS_RECEIVE,
  USER_REQUEST,
  USER_RECEIVE
} from '../types'
import failure from './failure'

const BASE_URL = 'https://jsonplaceholder.typicode.com'

const usersRequest = () => ({
  type: USERS_REQUEST
})

const usersReceive = (users) => ({
  type: USERS_RECEIVE,
  users
})

export const usersFetch = () => {
  return async (dispatch) => {
    dispatch(usersRequest())

    const [err, response] = await catchify(fetch(`${BASE_URL}/users`))

    if (err) {
      dispatch(failure(USERS_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(failure(USERS_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(usersReceive(data))
    }
  }
}

const userRequest = (userId) => ({
  type: USER_REQUEST,
  userId
})

const userReceive = (userId, userDetails) => ({
  type: USER_RECEIVE,
  userId,
  details: userDetails
})

export const userFetch = (userId) => {
  return async (dispatch) => {
    dispatch(userRequest(userId))

    const [err, response] = await catchify(fetch(`${BASE_URL}/users/${userId}`))

    if (err) {
      dispatch(failure(USER_REQUEST, err.message))
      return
    }

    const [errResp, data] = await catchify(response.json())

    if (errResp) {
      dispatch(failure(USER_REQUEST, errResp.message))
      return
    }

    if (data) {
      dispatch(userReceive(userId, data))
    }
  }
}
