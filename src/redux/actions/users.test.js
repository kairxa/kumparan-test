import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import { usersFetch, userFetch } from './users'
import { USERS_REQUEST, USERS_RECEIVE, USER_REQUEST, USER_RECEIVE, ACTION_FAILURE } from '../types'

const BASE_URL = 'https://jsonplaceholder.typicode.com'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Users fetch', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates USERS_RECEIVE when fetching users is done succesfully', () => {
    const users = [{
      'id': 1,
      'name': 'Leanne Graham',
      'username': 'Bret',
      'email': 'Sincere@april.biz',
      'address': {
        'street': 'Kulas Light',
        'suite': 'Apt. 556',
        'city': 'Gwenborough',
        'zipcode': '92998-3874',
        'geo': {
          'lat': '-37.3159',
          'lng': '81.1496'
        }
      },
      'phone': '1-770-736-8031 x56442',
      'website': 'hildegard.org',
      'company': {
        'name': 'Romaguera-Crona',
        'catchPhrase': 'Multi-layered client-server neural-net',
        'bs': 'harness real-time e-markets'
      }
    }]

    fetchMock.getOnce(`${BASE_URL}/users`, users)

    const expectedActions = [
      { type: USERS_REQUEST },
      { type: USERS_RECEIVE, users }
    ]

    const store = mockStore({ users: [] })

    return store.dispatch(usersFetch()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    fetchMock
      .getOnce(`${BASE_URL}/users`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: USERS_REQUEST },
      { type: ACTION_FAILURE, during: USERS_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ users: [] })

    return store.dispatch(usersFetch()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    fetchMock
      .getOnce(`${BASE_URL}/users`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: USERS_REQUEST },
      { type: ACTION_FAILURE, during: USERS_REQUEST, message: `invalid json response body at ${BASE_URL}/users reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ users: [] })

    return store.dispatch(usersFetch()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe('User fetch', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates USER_RECEIVE when everything is ok', () => {
    const user = {
      'id': 1,
      'name': 'Leanne Graham',
      'username': 'Bret',
      'email': 'Sincere@april.biz',
      'address': {
        'street': 'Kulas Light',
        'suite': 'Apt. 556',
        'city': 'Gwenborough',
        'zipcode': '92998-3874',
        'geo': {
          'lat': '-37.3159',
          'lng': '81.1496'
        }
      },
      'phone': '1-770-736-8031 x56442',
      'website': 'hildegard.org',
      'company': {
        'name': 'Romaguera-Crona',
        'catchPhrase': 'Multi-layered client-server neural-net',
        'bs': 'harness real-time e-markets'
      }
    }

    fetchMock.getOnce(`${BASE_URL}/users/1`, user)

    const expectedActions = [
      { type: USER_REQUEST, userId: 1 },
      { type: USER_RECEIVE, userId: 1, details: user }
    ]

    const store = mockStore({ type: '', userId: 0, details: {} })

    return store.dispatch(userFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when server returns error', () => {
    fetchMock
      .getOnce(`${BASE_URL}/users/1`, { throws: new Error('ERROR') })

    const expectedActions = [
      { type: USER_REQUEST, userId: 1 },
      { type: ACTION_FAILURE, during: USER_REQUEST, message: 'ERROR' }
    ]

    const store = mockStore({ type: '', userId: 0, details: {} })

    return store.dispatch(userFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates ACTION_FAILURE when response is not json', () => {
    fetchMock
      .getOnce(`${BASE_URL}/users/1`, 'RESPONSE IS NOT JSON')

    const expectedActions = [
      { type: USER_REQUEST, userId: 1 },
      { type: ACTION_FAILURE, during: USER_REQUEST, message: `invalid json response body at ${BASE_URL}/users/1 reason: Unexpected token R in JSON at position 0` }
    ]

    const store = mockStore({ type: '', userId: 0, details: {} })

    return store.dispatch(userFetch(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
