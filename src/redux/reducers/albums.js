import {
  ALBUM_LIST_REQUEST,
  ALBUM_LIST_RECEIVE,
  ALBUM_DETAIL_REQUEST,
  ALBUM_DETAIL_RECEIVE
} from '../types'

export const albums = (state = {
  userId: 0,
  albums: []
}, action) => {
  switch (action.type) {
    case ALBUM_LIST_REQUEST:
      return { ...state, userId: action.userId }
    case ALBUM_LIST_RECEIVE:
      return { ...state, userId: action.userId, albums: [...action.albums] }
    default:
      return state
  }
}

export const album = (state = {
  albumId: 0,
  details: {}
}, action) => {
  switch (action.type) {
    case ALBUM_DETAIL_REQUEST:
      return { ...state, albumId: action.albumId }
    case ALBUM_DETAIL_RECEIVE:
      return { ...state, albumId: action.albumId, details: { ...action.details } }
    default:
      return state
  }
}
