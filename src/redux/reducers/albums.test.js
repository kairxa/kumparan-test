import { albums as albumsReducer, album as albumReducer } from './albums'
import {
  ALBUM_LIST_REQUEST,
  ALBUM_LIST_RECEIVE,
  ALBUM_DETAIL_REQUEST,
  ALBUM_DETAIL_RECEIVE
} from '../types'

describe('Albums reducer', () => {
  it('returns initial state', () => {
    expect(albumsReducer(undefined, {})).toEqual({
      userId: 0,
      albums: []
    })
  })

  it('handles ALBUM_LIST_REQUEST', () => {
    const action = {
      type: ALBUM_LIST_REQUEST,
      userId: 1
    }

    expect(albumsReducer(undefined, action)).toEqual({
      userId: 1,
      albums: []
    })
  })

  it('handles ALBUM_LIST_RECEIVE', () => {
    const action = {
      type: ALBUM_LIST_RECEIVE,
      userId: 1,
      albums: [{ id: 1 }, { id: 2 }]
    }

    expect(albumsReducer(undefined, action)).toEqual({
      userId: 1,
      albums: [{ id: 1 }, { id: 2 }]
    })
  })
})

describe('Album reducer', () => {
  it('returns initial state', () => {
    expect(albumReducer(undefined, {})).toEqual({
      albumId: 0,
      details: {}
    })
  })

  it('handles ALBUM_DETAIL_REQUEST', () => {
    const action = {
      type: ALBUM_DETAIL_REQUEST,
      albumId: 1
    }

    expect(albumReducer(undefined, action)).toEqual({
      albumId: 1,
      details: {}
    })
  })

  it('handles ALBUM_DETAIL_RECEIVE', () => {
    const action = {
      type: ALBUM_DETAIL_RECEIVE,
      albumId: 1,
      details: { a: 1, b: 2 }
    }

    expect(albumReducer(undefined, action)).toEqual({
      albumId: 1,
      details: { a: 1, b: 2 }
    })
  })
})
