import {
  COMMENTS_REQUEST,
  COMMENTS_RECEIVE,
  COMMENT_REQUEST,
  COMMENT_RECEIVE,
  COMMENT_DELETE_REQUEST,
  COMMENT_DELETE_RECEIVE,
  COMMENT_ADD_RECEIVE,
  COMMENT_ADD_REQUEST,
  COMMENT_PATCH_REQUEST,
  COMMENT_PATCH_RECEIVE
} from '../types'

export const comments = (state = {
  postId: 0,
  comments: []
}, action) => {
  switch (action.type) {
    case COMMENTS_REQUEST:
      return { ...state, postId: action.postId }
    case COMMENTS_RECEIVE:
      return { ...state, postId: action.postId, comments: [...action.comments] }
    default:
      return state
  }
}

export const comment = (state = {
  commentId: 0,
  details: {},
  inProgress: false,
  success: false,
  failed: false
}, action) => {
  switch (action.type) {
    case COMMENT_REQUEST:
      return { ...state, commentId: action.commentId, inProgress: action.inProgress, success: action.success }
    case COMMENT_ADD_REQUEST:
      return {
        ...state,
        details: action.details,
        inProgress: action.inProgress,
        success: action.success,
        failed: action.failed
      }
    case COMMENT_RECEIVE:
    case COMMENT_ADD_RECEIVE:
    case COMMENT_PATCH_REQUEST:
    case COMMENT_PATCH_RECEIVE:
      return {
        ...state,
        commentId: action.commentId,
        details: { ...action.details },
        inProgress: action.inProgress,
        success: action.success,
        failed: action.failed
      }
    default:
      return state
  }
}

export const commentDelete = (state = {
  commentId: 0,
  inProgress: false,
  success: false,
  failed: false
}, action) => {
  switch (action.type) {
    case COMMENT_DELETE_REQUEST:
    case COMMENT_DELETE_RECEIVE:
      return { ...state, commentId: action.commentId, inProgress: action.inProgress, success: action.success, failed: action.failed }
    default:
      return state
  }
}
