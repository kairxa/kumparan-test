import { comments as commentsReducer, comment as commentReducer, commentDelete as commentDeleteReducer } from './comments'
import {
  COMMENTS_REQUEST,
  COMMENTS_RECEIVE,
  COMMENT_REQUEST,
  COMMENT_RECEIVE,
  COMMENT_ADD_REQUEST,
  COMMENT_ADD_RECEIVE,
  COMMENT_PATCH_REQUEST,
  COMMENT_PATCH_RECEIVE,
  COMMENT_DELETE_REQUEST,
  COMMENT_DELETE_RECEIVE
} from '../types'

describe('Comments reducer', () => {
  it('returns initial state', () => {
    expect(commentsReducer(undefined, {})).toEqual({
      postId: 0,
      comments: []
    })
  })

  it('handles COMMENTS_REQUEST', () => {
    const action = {
      type: COMMENTS_REQUEST,
      postId: 1
    }

    expect(commentsReducer(undefined, action)).toEqual({
      postId: 1,
      comments: []
    })
  })

  it('handles COMMENTS_RECEIVE', () => {
    const action = {
      type: COMMENTS_RECEIVE,
      postId: 1,
      comments: [{ id: 1 }, { id: 2 }]
    }

    expect(commentsReducer(undefined, action)).toEqual({
      postId: 1,
      comments: [{ id: 1 }, { id: 2 }]
    })
  })
})

describe('Comment reducer', () => {
  it('returns initial state', () => {
    expect(commentReducer(undefined, {})).toEqual({
      commentId: 0,
      details: {},
      inProgress: false,
      success: false,
      failed: false
    })
  })

  it('handles COMMENT_REQUEST', () => {
    const action = {
      type: COMMENT_REQUEST,
      commentId: 1,
      inProgress: true,
      success: false,
      failed: false
    }

    expect(commentReducer(undefined, action)).toEqual({
      commentId: 1,
      details: {},
      inProgress: true,
      success: false,
      failed: false
    })
  })

  it('handles COMMENT_RECEIVE', () => {
    const action = {
      type: COMMENT_RECEIVE,
      commentId: 1,
      details: { a: 1, b: 2 }
    }

    expect(commentReducer(undefined, action)).toEqual({
      commentId: 1,
      details: { a: 1, b: 2 }
    })
  })

  it('handles COMMENT_ADD_REQUEST', () => {
    const action = {
      type: COMMENT_ADD_REQUEST,
      details: { a: 1, b: 2 }
    }

    expect(commentReducer(undefined, action)).toEqual({
      commentId: 0,
      details: { a: 1, b: 2 }
    })
  })

  it('handles COMMENT_ADD_RECEIVE', () => {
    const action = {
      type: COMMENT_ADD_RECEIVE,
      commentId: 1,
      details: { a: 1, b: 2 }
    }

    expect(commentReducer(undefined, action)).toEqual({
      commentId: 1,
      details: { a: 1, b: 2 }
    })
  })

  it('handles COMMENT_PATCH_REQUEST', () => {
    const action = {
      type: COMMENT_PATCH_REQUEST,
      commentId: 1,
      details: { c: 3 }
    }

    expect(commentReducer(undefined, action)).toEqual({
      commentId: 1,
      details: { c: 3 }
    })
  })

  it('handles COMMENT_PATCH_RECEIVE', () => {
    const action = {
      type: COMMENT_PATCH_RECEIVE,
      commentId: 1,
      details: { a: 1, b: 2, c: 3 }
    }

    expect(commentReducer(undefined, action)).toEqual({
      commentId: 1,
      details: { a: 1, b: 2, c: 3 }
    })
  })
})

describe('Comment delete reducer', () => {
  it('returns initial state', () => {
    expect(commentDeleteReducer(undefined, {})).toEqual({
      commentId: 0,
      inProgress: false,
      success: false,
      failed: false
    })
  })

  it('handles COMMENT_DELETE_REQUEST', () => {
    const action = {
      type: COMMENT_DELETE_REQUEST,
      commentId: 1,
      inProgress: true,
      success: false,
      failed: false
    }

    expect(commentDeleteReducer(undefined, action)).toEqual({
      commentId: 1,
      inProgress: true,
      success: false,
      failed: false
    })
  })

  it('handles COMMENT_DELETE_RECEIVE', () => {
    const action = {
      type: COMMENT_DELETE_RECEIVE,
      commentId: 1,
      inProgress: false,
      success: true,
      failed: false
    }

    expect(commentDeleteReducer(undefined, action)).toEqual({
      commentId: 1,
      inProgress: false,
      success: true,
      failed: false
    })
  })
})
