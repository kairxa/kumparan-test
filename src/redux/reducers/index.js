import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { albums, album } from './albums'
import { comments, comment, commentDelete } from './comments'
import { photos, photo } from './photos'
import { posts, post, postDelete } from './posts'
import { users, user } from './users'

const rootReducer = combineReducers({
  albums,
  album,
  comments,
  comment,
  commentDelete,
  photos,
  photo,
  posts,
  post,
  postDelete,
  users,
  user,
  form: formReducer
})

export default rootReducer
