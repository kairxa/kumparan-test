import {
  PHOTOS_REQUEST,
  PHOTOS_RECEIVE,
  PHOTO_REQUEST,
  PHOTO_RECEIVE
} from '../types'

export const photos = (state = {
  albumId: 0,
  photos: []
}, action) => {
  switch (action.type) {
    case PHOTOS_REQUEST:
      return { ...state, albumId: action.albumId }
    case PHOTOS_RECEIVE:
      return { ...state, albumId: action.albumId, photos: [...action.photos] }
    default:
      return state
  }
}

export const photo = (state = {
  photoId: 0,
  details: {}
}, action) => {
  switch (action.type) {
    case PHOTO_REQUEST:
      return { ...state, photoId: action.photoId }
    case PHOTO_RECEIVE:
      return { ...state, photoId: action.photoId, details: { ...action.details } }
    default:
      return state
  }
}
