import { photos as photosReducer, photo as photoReducer } from './photos'
import {
  PHOTOS_REQUEST,
  PHOTOS_RECEIVE,
  PHOTO_REQUEST,
  PHOTO_RECEIVE
} from '../types'

describe('Photos reducer', () => {
  it('returns initial state', () => {
    expect(photosReducer(undefined, {})).toEqual({
      albumId: 0,
      photos: []
    })
  })

  it('handles PHOTOS_REQUEST', () => {
    const action = {
      type: PHOTOS_REQUEST,
      albumId: 1
    }

    expect(photosReducer(undefined, action)).toEqual({
      albumId: 1,
      photos: []
    })
  })

  it('handles PHOTOS_RECEIVE', () => {
    const action = {
      type: PHOTOS_RECEIVE,
      albumId: 1,
      photos: [{ id: 1 }, { id: 2 }]
    }

    expect(photosReducer(undefined, action)).toEqual({
      albumId: 1,
      photos: [{ id: 1 }, { id: 2 }]
    })
  })
})

describe('Photo reducer', () => {
  it('returns initial state', () => {
    expect(photoReducer(undefined, {})).toEqual({
      photoId: 0,
      details: {}
    })
  })

  it('handles PHOTO_REQUEST', () => {
    const action = {
      type: PHOTO_REQUEST,
      photoId: 1
    }

    expect(photoReducer(undefined, action)).toEqual({
      photoId: 1,
      details: {}
    })
  })

  it('handles PHOTO_RECEIVE', () => {
    const action = {
      type: PHOTO_RECEIVE,
      photoId: 1,
      details: { a: 1, b: 2 }
    }

    expect(photoReducer(undefined, action)).toEqual({
      photoId: 1,
      details: { a: 1, b: 2 }
    })
  })
})
