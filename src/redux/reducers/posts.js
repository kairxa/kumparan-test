import {
  POSTS_REQUEST,
  POSTS_RECEIVE,
  POST_REQUEST,
  POST_RECEIVE,
  POST_DELETE_REQUEST,
  POST_DELETE_RECEIVE,
  POST_ADD_RECEIVE,
  POST_ADD_REQUEST,
  POST_PATCH_REQUEST,
  POST_PATCH_RECEIVE
} from '../types'

export const posts = (state = {
  userId: 0,
  posts: []
}, action) => {
  switch (action.type) {
    case POSTS_REQUEST:
      return { ...state, userId: action.userId }
    case POSTS_RECEIVE:
      return { ...state, userId: action.userId, posts: [...action.posts] }
    default:
      return state
  }
}

export const post = (state = {
  postId: 0,
  details: {},
  inProgress: false,
  success: false,
  failed: false
}, action) => {
  switch (action.type) {
    case POST_REQUEST:
      return { ...state, postId: action.postId, inProgress: action.inProgress, success: action.success }
    case POST_ADD_REQUEST:
      return {
        ...state,
        details: action.details,
        inProgress: action.inProgress,
        success: action.success,
        failed: action.failed
      }
    case POST_RECEIVE:
    case POST_ADD_RECEIVE:
    case POST_PATCH_REQUEST:
    case POST_PATCH_RECEIVE:
      return {
        ...state,
        postId: action.postId,
        details: { ...action.details },
        inProgress: action.inProgress,
        success: action.success,
        failed: action.failed
      }
    default:
      return state
  }
}

export const postDelete = (state = {
  postId: 0,
  inProgress: false,
  success: false,
  failed: false
}, action) => {
  switch (action.type) {
    case POST_DELETE_REQUEST:
    case POST_DELETE_RECEIVE:
      return { ...state, postId: action.postId, inProgress: action.inProgress, success: action.success, failed: action.failed }
    default:
      return state
  }
}
