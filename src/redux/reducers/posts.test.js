import { posts as postsReducer, post as postReducer, postDelete as postDeleteReducer } from './posts'
import {
  POSTS_REQUEST,
  POSTS_RECEIVE,
  POST_REQUEST,
  POST_RECEIVE,
  POST_ADD_REQUEST,
  POST_ADD_RECEIVE,
  POST_PATCH_REQUEST,
  POST_PATCH_RECEIVE,
  POST_DELETE_REQUEST,
  POST_DELETE_RECEIVE
} from '../types'

describe('Posts reducer', () => {
  it('returns initial state', () => {
    expect(postsReducer(undefined, {})).toEqual({
      userId: 0,
      posts: []
    })
  })

  it('handles POSTS_REQUEST', () => {
    const action = {
      type: POSTS_REQUEST,
      userId: 1
    }

    expect(postsReducer(undefined, action)).toEqual({
      userId: 1,
      posts: []
    })
  })

  it('handles POSTS_RECEIVE', () => {
    const action = {
      type: POSTS_RECEIVE,
      userId: 1,
      posts: [{ id: 1 }, { id: 2 }]
    }

    expect(postsReducer(undefined, action)).toEqual({
      userId: 1,
      posts: [{ id: 1 }, { id: 2 }]
    })
  })
})

describe('Post reducer', () => {
  it('returns initial state', () => {
    expect(postReducer(undefined, {})).toEqual({
      postId: 0,
      details: {},
      inProgress: false,
      success: false,
      failed: false
    })
  })

  it('handles POST_REQUEST', () => {
    const action = {
      type: POST_REQUEST,
      postId: 1,
      inProgress: true,
      success: false,
      failed: false
    }

    expect(postReducer(undefined, action)).toEqual({
      postId: 1,
      details: {},
      inProgress: true,
      success: false,
      failed: false
    })
  })

  it('handles POST_RECEIVE', () => {
    const action = {
      type: POST_RECEIVE,
      postId: 1,
      details: { a: 1, b: 2 },
      inProgress: false,
      success: true,
      failed: false
    }

    expect(postReducer(undefined, action)).toEqual({
      postId: 1,
      details: { a: 1, b: 2 },
      inProgress: false,
      success: true,
      failed: false
    })
  })

  it('handles POST_ADD_REQUEST', () => {
    const action = {
      type: POST_ADD_REQUEST,
      details: { a: 1, b: 2 },
      inProgress: true,
      success: false,
      failed: false
    }

    expect(postReducer(undefined, action)).toEqual({
      postId: 0,
      details: { a: 1, b: 2 },
      inProgress: true,
      success: false,
      failed: false
    })
  })

  it('handles POST_ADD_RECEIVE', () => {
    const action = {
      type: POST_ADD_RECEIVE,
      postId: 1,
      details: { a: 1, b: 2 },
      inProgress: false,
      success: true,
      failed: false
    }

    expect(postReducer(undefined, action)).toEqual({
      postId: 1,
      details: { a: 1, b: 2 },
      inProgress: false,
      success: true,
      failed: false
    })
  })

  it('handles POST_PATCH_REQUEST', () => {
    const action = {
      type: POST_PATCH_REQUEST,
      postId: 1,
      details: { c: 3 },
      inProgress: true,
      success: false,
      failed: false
    }

    expect(postReducer(undefined, action)).toEqual({
      postId: 1,
      details: { c: 3 },
      inProgress: true,
      success: false,
      failed: false
    })
  })

  it('handles POST_PATCH_RECEIVE', () => {
    const action = {
      type: POST_PATCH_RECEIVE,
      postId: 1,
      details: { a: 1, b: 2, c: 3 },
      inProgress: false,
      success: true,
      failed: false
    }

    expect(postReducer(undefined, action)).toEqual({
      postId: 1,
      details: { a: 1, b: 2, c: 3 },
      inProgress: false,
      success: true,
      failed: false
    })
  })
})

describe('Post delete reducer', () => {
  it('returns initial state', () => {
    expect(postDeleteReducer(undefined, {})).toEqual({
      postId: 0,
      inProgress: false,
      success: false,
      failed: false
    })
  })

  it('handles POST_DELETE_REQUEST', () => {
    const action = {
      type: POST_DELETE_REQUEST,
      postId: 1,
      inProgress: true,
      success: false,
      failed: false
    }

    expect(postDeleteReducer(undefined, action)).toEqual({
      postId: 1,
      inProgress: true,
      success: false,
      failed: false
    })
  })

  it('handles POST_DELETE_RECEIVE', () => {
    const action = {
      type: POST_DELETE_RECEIVE,
      postId: 1,
      inProgress: false,
      success: true,
      failed: false
    }

    expect(postDeleteReducer(undefined, action)).toEqual({
      postId: 1,
      inProgress: false,
      success: true,
      failed: false
    })
  })
})
