import {
  USERS_REQUEST,
  USERS_RECEIVE,
  USER_REQUEST,
  USER_RECEIVE
} from '../types'

export const users = (state = {
  users: []
}, action) => {
  switch (action.type) {
    case USERS_REQUEST:
      return { ...state }
    case USERS_RECEIVE:
      return { ...state, users: [...action.users] }
    default:
      return state
  }
}

export const user = (state = {
  userId: 0,
  details: {
    address: {},
    company: {}
  }
}, action) => {
  switch (action.type) {
    case USER_REQUEST:
      return { ...state, userId: action.userId }
    case USER_RECEIVE:
      return { ...state, userId: action.userId, details: { ...action.details } }
    default:
      return state
  }
}
