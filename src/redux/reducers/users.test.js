import { users as usersReducer, user as userReducer } from './users'
import {
  USERS_REQUEST,
  USERS_RECEIVE,
  USER_REQUEST,
  USER_RECEIVE
} from '../types'

describe('Users reducer', () => {
  it('returns initial state', () => {
    expect(usersReducer(undefined, {})).toEqual({
      users: []
    })
  })

  it('handles USERS_REQUEST', () => {
    const action = {
      type: USERS_REQUEST
    }

    expect(usersReducer(undefined, action)).toEqual({
      users: []
    })
  })

  it('handles USERS_RECEIVE', () => {
    const action = {
      type: USERS_RECEIVE,
      users: [{ id: 1 }, { id: 2 }]
    }

    expect(usersReducer(undefined, action)).toEqual({
      users: [{ id: 1 }, { id: 2 }]
    })
  })
})

describe('User reducer', () => {
  it('returns initial state', () => {
    expect(userReducer(undefined, {})).toEqual({
      userId: 0,
      details: { address: {}, company: {} }
    })
  })

  it('handles USER_REQUEST', () => {
    const action = {
      type: USER_REQUEST,
      userId: 1
    }

    expect(userReducer(undefined, action)).toEqual({
      userId: 1,
      details: { address: {}, company: {} }
    })
  })

  it('handles USER_RECEIVE', () => {
    const action = {
      type: USER_RECEIVE,
      userId: 1,
      details: { a: 1, b: 2 }
    }

    expect(userReducer(undefined, action)).toEqual({
      userId: 1,
      details: { a: 1, b: 2 }
    })
  })
})
