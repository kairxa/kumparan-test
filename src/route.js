import Users from './views/Users'
import User from './views/User'
import Posts from './views/Posts'
import Comments from './views/Comments'
import PostEditor from './views/PostEditor'
import PostDelete from './views/PostDelete'
import CommentEditor from './views/CommentEditor'
import CommentDelete from './views/CommentDelete'
import Albums from './views/Albums'
import Photos from './views/Photos'
import Photo from './views/Photo'

export default [
  {
    path: '/users',
    // Shows a list of users
    component: Users
  },
  {
    path: '/user/:userId',
    // Shows a specific user
    component: User,
    routes: [
      {
        path: '/user/:userId/albums',
        // Shows a list of albums from specific user
        component: Albums
      },
      {
        path: '/user/:userId/posts',
        // Shows a list of posts from specific user
        component: Posts
      }
    ]
  },
  {
    path: '/album/:albumId/photos',
    component: Photos
  },
  {
    path: '/post/:postId/comments',
    component: Comments
  },
  {
    path: '/post/add',
    // Add a new post; shares the same view with /post/edit
    component: PostEditor
  },
  {
    path: '/post/:postId/edit',
    component: PostEditor
  },
  {
    path: '/post/:postId/delete',
    component: PostDelete
  },
  {
    path: '/comment/add',
    component: CommentEditor
  },
  {
    path: '/comment/:commentId/edit',
    component: CommentEditor
  },
  {
    path: '/comment/:commentId/delete',
    component: CommentDelete
  },
  {
    path: '/photo/:photoId',
    // Shows a specific photo detail
    component: Photo
  }
]
