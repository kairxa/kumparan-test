import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { albumsFetch } from '../redux/actions'
import AlbumCard from '../components/AlbumCard'

class Albums extends Component {
  componentDidMount () {
    const { dispatch, match } = this.props
    const { userId } = match.params

    dispatch(albumsFetch(userId))
  }

  render () {
    return (
      <section className="flex flex-col flex-1 w-full max-w-xl p-4 lg:pr-0 lg:pl-0">
        {this.props.albums.map((album, i) => (
          <AlbumCard key={i} album={album}></AlbumCard>
        ))}
      </section>
    )
  }
}

Albums.propTypes = {
  albums: PropTypes.arrayOf(PropTypes.shape({
    userId: PropTypes.number,
    id: PropTypes.number,
    title: PropTypes.string
  })),
  match: PropTypes.shape({
    params: PropTypes.shape({
      userId: PropTypes.string
    })
  }).isRequired,
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = ({ albums }) => ({ albums: albums.albums })

export default connect(mapStateToProps)(Albums)
