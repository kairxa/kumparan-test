import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { commentFetch, commentDelete } from '../redux/actions'
import CommentCard from '../components/CommentCard'

class CommentDelete extends Component {
  sureDelete = () => {
    const { dispatch, match } = this.props
    const { commentId } = match.params

    dispatch(commentDelete(commentId))
  }

  componentDidMount () {
    const { dispatch, match } = this.props
    const { commentId } = match.params

    dispatch(commentFetch(commentId))
  }

  render () {
    const { comment, inProgress, success, failed } = this.props

    return (
      <section className="flex flex-col flex-1 w-full max-w-xl p-4 lg:pr-0 lg:pl-0">
        <CommentCard comment={comment}></CommentCard>
        <section className="flex flex-col bg-white shadow w-full p-4 lg:rounded">
          <span className="font-sans font-bold text-black text-base">
            Are you sure you want to delete this?
          </span>
          <div className="mt-4 flex items-center">
            <button className="button-Base mod--seriousAction" onClick={this.sureDelete}>Yes</button>
            <Link className="flex items-center justify-center no-underline button-Base mod--reset" to="/users">No</Link>
          </div>
          {(inProgress || success || failed) &&
          <div className="mt-8 flex justify-center">
            {!inProgress && success && <span className="font-sans font-bold text-base text-black">Sukses hapus!</span>}
            {inProgress && !success && <span className="font-sans font-bold text-base text-black">Sedang hapus..</span>}
            {failed && <span className="font-sans font-bold text-base text-black">Gagal!</span>}
          </div>
          }
        </section>
      </section>
    )
  }
}

CommentDelete.propTypes = {
  comment: PropTypes.shape({
    id: PropTypes.number,
    postId: PropTypes.number,
    name: PropTypes.string,
    body: PropTypes.string,
    email: PropTypes.string
  }),
  match: PropTypes.shape({
    params: PropTypes.shape({
      commentId: PropTypes.string
    })
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
  inProgress: PropTypes.bool,
  success: PropTypes.bool,
  failed: PropTypes.bool
}

const mapStateToProps = ({ comment, commentDelete }) => ({
  comment: comment.details,
  inProgress: commentDelete.inProgress,
  success: commentDelete.success,
  failed: commentDelete.failed
})

export default connect(mapStateToProps)(CommentDelete)
