import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { commentFetch, commentAdd, commentUpdate } from '../redux/actions'
import CommentForm from '../components/CommentForm'

class CommentEditor extends Component {
  submit = (values) => {
    const { dispatch, match } = this.props
    const { commentId } = match.params
    let sentValues = { ...values }
    let dispatchFunc = commentAdd

    if (/edit$/.test(match.url)) {
      dispatchFunc = commentUpdate
      sentValues = { ...sentValues, id: commentId }
    }

    dispatch(dispatchFunc(sentValues))
  }

  componentDidMount () {
    if (/edit$/.test(this.props.match.url)) {
      const { dispatch, match } = this.props
      const { commentId } = match.params

      dispatch(commentFetch(commentId))
    }
  }

  render () {
    const { inProgress, success, failed } = this.props

    return (
      <section className="flex flex-1 w-full max-w-xl p-4 lg:pr-0 lg:pl-0">
        <section className="flex-1 w-full h-full min-h-full bg-white lg:rounded shadow p-4">
          <CommentForm onSubmit={this.submit}></CommentForm>
          {(inProgress || success || failed) &&
          <div className="mt-8 flex justify-center">
            {!inProgress && success && <span className="font-sans font-bold text-base text-black">Sukses comment!</span>}
            {inProgress && !success && <span className="font-sans font-bold text-base text-black">Sedang comment..</span>}
            {failed && <span className="font-sans font-bold text-base text-black">Gagal!</span>}
          </div>
          }
        </section>
      </section>
    )
  }
}

CommentEditor.propTypes = {
  inProgress: PropTypes.bool,
  success: PropTypes.bool,
  failed: PropTypes.bool,
  match: PropTypes.shape({
    params: PropTypes.shape({
      commentId: PropTypes.string
    }),
    url: PropTypes.string
  }),
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = ({ comment }) => ({ inProgress: comment.inProgress, success: comment.success, failed: comment.failed })

export default connect(mapStateToProps)(CommentEditor)
