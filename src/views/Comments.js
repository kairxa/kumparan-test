import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { postFetch, commentsFetch } from '../redux/actions'
import PostCard from '../components/PostCard'
import CommentCard from '../components/CommentCard'

class Comments extends Component {
  componentDidMount () {
    const { dispatch, match } = this.props
    const { postId } = match.params

    dispatch(postFetch(postId))
    dispatch(commentsFetch(postId))
  }

  render () {
    return (
      <section className="flex flex-col flex-1 w-full max-w-xl p-4 lg:pr-0 lg:pl-0">
        <PostCard post={this.props.post}></PostCard>

        {this.props.comments.map((comment, i) => (
          <CommentCard key={i} comment={comment}></CommentCard>
        ))}
      </section>
    )
  }
}

Comments.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.number,
    userId: PropTypes.number,
    title: PropTypes.string,
    body: PropTypes.string
  }),
  comments: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    postId: PropTypes.number,
    name: PropTypes.string,
    email: PropTypes.string,
    body: PropTypes.string
  })),
  match: PropTypes.shape({
    params: PropTypes.shape({
      postId: PropTypes.string
    })
  }).isRequired,
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = ({ post, comments }) => ({ post: post.details, comments: comments.comments })

export default connect(mapStateToProps)(Comments)
