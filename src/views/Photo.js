import React, { Component } from 'react'
import { connect } from 'react-redux'
import { photoFetch } from '../redux/actions'
import PropTypes from 'prop-types'
class Photo extends Component {
  componentDidMount () {
    const { dispatch, match } = this.props
    const { photoId } = match.params

    dispatch(photoFetch(photoId))
  }

  render () {
    const { photo } = this.props

    return (
      <section className="flex flex-col flex-1 w-full max-w-xl p-4 lg:pr-0 lg:pl-0">
        <div className="flex justify-center">
          <img src={photo.url} alt={photo.title} className="block"/>
        </div>
        <div className="mt-4 flex flex-col flex-1 items-center">
          <span className="font-sans font-bold text-black text-base">{photo.title}</span>
        </div>
      </section>
    )
  }
}

Photo.propTypes = {
  photo: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    url: PropTypes.string,
    thumbnailUrl: PropTypes.string
  }),
  match: PropTypes.shape({
    params: PropTypes.shape({
      photoId: PropTypes.string
    })
  }),
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = ({ photo }) => ({ photo: photo.details })

export default connect(mapStateToProps)(Photo)
