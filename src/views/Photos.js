import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { albumFetch, photosFetch } from '../redux/actions'
import AlbumCard from '../components/AlbumCard'
import PhotoThumb from '../components/PhotoThumb'

class Photos extends Component {
  componentDidMount () {
    const { dispatch, match } = this.props
    const { albumId } = match.params

    dispatch(albumFetch(albumId))
    dispatch(photosFetch(albumId))
  }

  render () {
    return (
      <section className="flex flex-wrap w-full max-w-xl p-4 justify-center lg:pr-0 lg:pl-0">
        <AlbumCard album={this.props.album}></AlbumCard>

        {this.props.photos.map((photo, i) => (
          <PhotoThumb key={i} photo={photo}></PhotoThumb>
        ))}
      </section>
    )
  }
}

Photos.propTypes = {
  album: PropTypes.shape({
    userId: PropTypes.number,
    id: PropTypes.number,
    title: PropTypes.string
  }),
  photos: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    albumId: PropTypes.number,
    url: PropTypes.string,
    title: PropTypes.string,
    thumbnailUrl: PropTypes.string
  })),
  match: PropTypes.shape({
    params: PropTypes.shape({
      albumId: PropTypes.string
    })
  }).isRequired,
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = ({ album, photos }) => ({ album: album.details, photos: photos.photos })

export default connect(mapStateToProps)(Photos)
