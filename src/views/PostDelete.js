import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { postFetch, postDelete } from '../redux/actions'
import PostCard from '../components/PostCard'

class PostDelete extends Component {
  sureDelete = () => {
    const { dispatch, match } = this.props
    const { postId } = match.params

    dispatch(postDelete(postId))
  }

  componentDidMount () {
    const { dispatch, match } = this.props
    const { postId } = match.params

    dispatch(postFetch(postId))
  }

  render () {
    const { post, inProgress, success, failed } = this.props

    return (
      <section className="flex flex-col flex-1 w-full max-w-xl p-4 lg:pr-0 lg:pl-0">
        <PostCard post={post}></PostCard>
        <section className="flex flex-col bg-white shadow w-full p-4 lg:rounded">
          <span className="font-sans font-bold text-black text-base">
            Are you sure you want to delete this?
          </span>
          <div className="mt-4 flex items-center">
            <button className="button-Base mod--seriousAction" onClick={this.sureDelete}>Yes</button>
            <Link className="flex items-center justify-center no-underline button-Base mod--reset" to="/users">No</Link>
          </div>
          {(inProgress || success || failed) &&
          <div className="mt-8 flex justify-center">
            {!inProgress && success && <span className="font-sans font-bold text-base text-black">Sukses hapus!</span>}
            {inProgress && !success && <span className="font-sans font-bold text-base text-black">Sedang hapus..</span>}
            {failed && <span className="font-sans font-bold text-base text-black">Gagal!</span>}
          </div>
          }
        </section>
      </section>
    )
  }
}

PostDelete.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.number,
    userId: PropTypes.number,
    title: PropTypes.string,
    body: PropTypes.string
  }),
  match: PropTypes.shape({
    params: PropTypes.shape({
      postId: PropTypes.string
    })
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
  inProgress: PropTypes.bool,
  success: PropTypes.bool,
  failed: PropTypes.bool
}

const mapStateToProps = ({ post, postDelete }) => ({
  post: post.details,
  inProgress: postDelete.inProgress,
  success: postDelete.success,
  failed: postDelete.failed
})

export default connect(mapStateToProps)(PostDelete)
