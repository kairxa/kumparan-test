import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { postFetch, postAdd, postUpdate } from '../redux/actions'
import PostForm from '../components/PostForm'
class PostEditor extends Component {
  submit = (values) => {
    const { dispatch, match } = this.props
    const { postId } = match.params
    let sentValues = { ...values }
    let dispatchFunc = postAdd

    if (/edit$/.test(match.url)) {
      dispatchFunc = postUpdate
      sentValues = { ...sentValues, id: postId }
    }

    dispatch(dispatchFunc(sentValues))
  }

  componentDidMount () {
    if (/edit$/.test(this.props.match.url)) {
      const { dispatch, match } = this.props
      const { postId } = match.params

      dispatch(postFetch(postId))
    }
  }

  render () {
    const { inProgress, success, failed } = this.props

    return (
      <section className="flex flex-1 w-full max-w-xl p-4 lg:pr-0 lg:pl-0">
        <section className="flex-1 w-full h-full min-h-full bg-white lg:rounded shadow p-4">
          <PostForm onSubmit={this.submit}></PostForm>
          {(inProgress || success || failed) &&
          <div className="mt-8 flex justify-center">
            {!inProgress && success && <span className="font-sans font-bold text-base text-black">Sukses post!</span>}
            {inProgress && !success && <span className="font-sans font-bold text-base text-black">Sedang post..</span>}
            {failed && <span className="font-sans font-bold text-base text-black">Gagal!</span>}
          </div>
          }
        </section>
      </section>
    )
  }
}

PostEditor.propTypes = {
  inProgress: PropTypes.bool,
  success: PropTypes.bool,
  failed: PropTypes.bool,
  match: PropTypes.shape({
    params: PropTypes.shape({
      postId: PropTypes.string
    }),
    url: PropTypes.string
  }),
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = ({ post }) => ({ inProgress: post.inProgress, success: post.success, failed: post.failed })

export default connect(mapStateToProps)(PostEditor)
