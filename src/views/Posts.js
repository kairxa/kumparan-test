import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { postsFetch } from '../redux/actions'
import PostCard from '../components/PostCard'

class Posts extends Component {
  componentDidMount () {
    const { dispatch, match } = this.props
    const { userId } = match.params

    dispatch(postsFetch(userId))
  }

  render () {
    return (
      <section className="mt-5 flex flex-col">
        {this.props.posts.map((post, i) => (
          <PostCard key={i} post={post}></PostCard>
        ))}
      </section>
    )
  }
}

Posts.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    userId: PropTypes.number,
    title: PropTypes.string,
    body: PropTypes.string
  })),
  match: PropTypes.shape({
    params: PropTypes.shape({
      userId: PropTypes.string
    })
  }),
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = ({ posts }) => ({ posts: posts.posts })

export default connect(mapStateToProps)(Posts)
