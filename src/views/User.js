import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { userFetch } from '../redux/actions'
import RouteSub from '../components/RouteSub'
import UserCard from '../components/UserCard'

class User extends Component {
  componentDidMount () {
    const { dispatch, match } = this.props
    const { userId } = match.params

    dispatch(userFetch(userId))
  }

  render () {
    return (
      <section className="flex flex-col flex-1 w-full max-w-xl p-4 lg:pr-0 lg:pl-0">
        <UserCard user={this.props.user}></UserCard>
        <div className="mt-4 w-full">
          <Link className="button-Base mod--submit flex justify-center items-center no-underline" to="/post/add">Add Post</Link>
        </div>

        {this.props.routes.map((route, i) => (
          <RouteSub key={i} {...route}></RouteSub>
        ))}
      </section>
    )
  }
}

User.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    username: PropTypes.string,
    email: PropTypes.string,
    address: PropTypes.shape({
      street: PropTypes.string,
      suite: PropTypes.string,
      city: PropTypes.string
    }),
    phone: PropTypes.string,
    website: PropTypes.string,
    company: PropTypes.shape({
      name: PropTypes.string,
      catchPhrase: PropTypes.string,
      bs: PropTypes.string
    })
  }),
  routes: PropTypes.arrayOf(PropTypes.shape),
  match: PropTypes.shape({
    params: PropTypes.shape({
      userId: PropTypes.string
    })
  }),
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = ({ user }) => ({ user: user.details })

export default connect(mapStateToProps)(User)
