import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { usersFetch } from '../redux/actions'
import UserCard from '../components/UserCard'

class Users extends Component {
  componentDidMount () {
    const { dispatch } = this.props

    dispatch(usersFetch())
  }

  render () {
    return (
      <section className="flex flex-col flex-1 w-full max-w-xl p-4 lg:pr-0 lg:pl-0">
        {this.props.users.map((user, i) => (
          <UserCard key={i} user={user}></UserCard>
        ))}
      </section>
    )
  }
}

Users.propTypes = {
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    username: PropTypes.string,
    email: PropTypes.string,
    address: PropTypes.shape({
      street: PropTypes.string,
      suite: PropTypes.string,
      city: PropTypes.string
    }),
    phone: PropTypes.string,
    website: PropTypes.string,
    company: PropTypes.shape({
      name: PropTypes.string,
      catchPhrase: PropTypes.string,
      bs: PropTypes.string
    })
  })),
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = ({ users }) => ({ users: users.users })

export default connect(mapStateToProps)(Users)
